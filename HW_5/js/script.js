///// 1 /////
function getParam() {
    let sum = 0
    return function result(res = 0) {
        return sum += res
    }
}
let counter = getParam()
console.log(counter(3))
console.log(counter(5))
console.log(counter(228))
///// 2 /////
function Updated() {
    let arr = []
    return function result(res) {
        if(res === undefined){
            arr = []
            return arr
        }else{
            arr.push(res)
            return arr
        }
    }
}
let getUpdatedArr = Updated()
console.log(getUpdatedArr(3)) // [3] 
console.log(getUpdatedArr(5)) // [3, 5]
console.log(getUpdatedArr({name: 'Vasya'})) // [3, 5, {name: 'Vasya'}]
console.log(getUpdatedArr()) // []
console.log(getUpdatedArr(4)) // [4]
///// 3 /////
const getTime = ((count=Math.floor(new Date().getTime()/ 1000)) => () =>{
    let a = Math.floor(new Date().getTime() / 1000)
    let b = a - count
    count = a
    return b
})()
///// 4 /////
const perseTime = (time) => {
    let seconds = time % 60
    let minutes = (time - seconds) / 60
    return `${minutes > 9 ? '' : '0'}${minutes} : ${seconds > 9 ? '' : '0'}${seconds}`
}

const timer = (time = 100) => {
    const timerId = setInterval (() => {
        time-- 
        if(time === 0){ 
            clearInterval(timerId)
            console.log('Time end!');
            return
        }
        console.log(perseTime(time));
    },1000)
}
timer()