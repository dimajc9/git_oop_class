class Student {
    constructor(enrollee){
    this.id = Student.id++;
    this.name = enrollee.name;
    this.surname = enrollee.surname;
    this.ratingPoint = enrollee.ratingPoint;
    this.schoolPoint = enrollee.schoolPoint;
    this.isSelfPayment = true;
    }
    static id = 1;
}
let createStudents = () => {
    return Array.from(studentArr);
}
let listOfStudents = [];
for (let key of createStudents()) {
    listOfStudents.push(new Student(key));
}
let sortByRatingPoint = listOfStudents.sort((a, b) =>{
    if (a.ratingPoint == b.ratingPoint) {
        return b.schoolPoint - a.schoolPoint;
    } else {
        return b.ratingPoint - a.ratingPoint;
    }
})
let minimalSort = sortByRatingPoint.filter((a) => (a.ratingPoint >= 800));
let paimantFalsGuys = minimalSort.slice(0,5);
for(let i = 0; i < sortByRatingPoint.length; i++){
    for(let j = 0; j < paimantFalsGuys.length; j++){
        if (sortByRatingPoint[i].id == paimantFalsGuys[j].id){
            sortByRatingPoint[i].isSelfPayment = false;
        }
    }
}
